<?php

require '../__connect_db.php';

include './value_match.php';

// 抓登入的餐廳的 id
$restaurant_id = $_SESSION['loginUser']['restaurant_id'];


$number = isset($_GET['number']) ? intval($_GET['number']) : 0;;
// echo $number;

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
// echo $_GET['page'];

$per_page = 5; // 每一頁要顯示幾筆

$t_sql = "SELECT COUNT(1) FROM `dinner_list`";

$t_stmt = $pdo->query($t_sql);
$totalRows = $t_stmt->fetch(PDO::FETCH_NUM)[0];

$totalPages = ceil($totalRows/$per_page);

if($page<1):
  $page = 1;
endif;

if($page>$totalPages):
  $page = $totalPages;
endif;

$result = [
    'page' => $page,
    'number' => $number,
    'per_page' => $per_page,
    'totalRows' => $totalRows,
    'totalPages' => $totalPages,
    'rows' => [],
    'search' => '',
    'checked_check' => '',
    'check' => '無',
];



// 關聯式陣列以逗號分隔 

  // $sql_f = "SELECT d.`dinner_id`, p.`class_sid`, p.`name`
  // FROM `dinner_list` AS d JOIN `product_class` AS p
  // WHERE `class_sid` IN (d.`main_ingred`, d.`main_ingred_replace1`, d.`main_ingred_replace2`, d.`main_ingred_replace3`) ORDER BY d.`dinner_id` ASC";

  // $stmt_f = $pdo->query($sql_f);
  // $row_f = $stmt_f->fetchAll(PDO::FETCH_NUM);
// print_r($row_f);

// 轉陣列
// $food = [];
// foreach ($row_f as $k => $v) {
//     $food += [$v[1] => $v[2]];
// };

// print_r($food);
// echo(count($food));

// echo '<pre>';
//       print_r($row_f);
//     echo '</pre>';

// $result['food_ingred'] = $stmt_f->fetchAll();

// 拿餐廳 id 和名稱
$sql = "SELECT r.`restaurant_id`, r.`name`
FROM `restaurant` AS r JOIN `dinner_list` AS d
WHERE r.`restaurant_id` IN (d.`restaurant_id`) ORDER BY r.restaurant_id ASC";

$stmt_restaurant = $pdo->query($sql);
$row_restaurant = $stmt_restaurant->fetchAll(PDO::FETCH_NUM);
// echo '<pre>';
//       print_r($row_restaurant);
//     echo '</pre>';

// 抓登入的餐廳有的菜色
// 生冪降冪
if($result['number']==0){
  $sql_r = sprintf("SELECT `dinner_id`, `restaurant_id`, `main_cat`, `small_cat`, `name`, `intro`, `dinner_image`, `onboard` FROM `dinner_list` WHERE `restaurant_id` = $restaurant_id ORDER BY `dinner_id` ASC LIMIT %s, %s",
  ($page-1)*$per_page, $per_page);
  
  $stmt_r = $pdo->query($sql_r);
  $row = $stmt_r->fetchAll();

  $result['rows'] = $row;
}else{
  $sql_r = sprintf("SELECT `dinner_id`, `restaurant_id`, `main_cat`, `small_cat`, `name`, `intro`, `dinner_image`, `onboard` FROM `dinner_list` WHERE `restaurant_id` = $restaurant_id  ORDER BY `dinner_id` DESC LIMIT %s, %s",
    
    ($page-1)*$per_page, $per_page);
    
    $stmt_r = $pdo->query($sql_r);
    $row = $stmt_r->fetchAll();

    $result['rows'] = $row;
  }


// 如果有搜尋

  if(! empty($_GET['searchName'])){

    $search = $_GET['searchName'];
    $search_num = $food_num[$search];

    // echo($search_num);   
    if($result['number']==0){
      $sql = sprintf("SELECT DISTINCT `dinner_list`.*
      FROM `dinner_list`
      JOIN `dinnerProduct` ON `dinner_list`.`dinner_id` = `dinnerProduct`.`dinner_list` WHERE
      `dinnerProduct`.`product_class` =  $search_num ORDER BY `dinner_id` ASC LIMIT %s, %s", ($page-1)*$per_page, $per_page);
    }else{
      $sql = sprintf("SELECT DISTINCT `dinner_list`.*
      FROM `dinner_list`
      JOIN `dinnerProduct` ON `dinner_list`.`dinner_id` = `dinnerProduct`.`dinner_list` WHERE
      `dinnerProduct`.`product_class` =  $search_num ORDER BY `dinner_id` DESC LIMIT %s, %s", ($page-1)*$per_page, $per_page);
    }
    
      $stmt = $pdo->query($sql);
      $row = $stmt->fetchAll();

      if($stmt->rowcount()==0){
        $result['search'] = '無資料';
        $result['rows'] = '';

        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
      };

      $result['rows'] = $row;
      $result['test'] = $search;
      // print_r($row);

      // echo json_encode($result, JSON_UNESCAPED_UNICODE);
      // exit;
  }

  // 如果有點按中、西式分類的篩選按鈕
  
  if(! empty($_GET['searchType'])){

    $search = $_GET['searchType'];

    if($result['number']==0){
      $sql = sprintf("SELECT DISTINCT `dinner_list`.*
      FROM `dinner_list`
      JOIN `dinnerProduct` ON `dinner_list`.`dinner_id` = `dinnerProduct`.`dinner_list` WHERE
      `dinner_list`.`main_cat` = '$search' ORDER BY `dinner_id` ASC LIMIT %s, %s", ($page-1)*$per_page, $per_page);
    }else{
      $sql = sprintf("SELECT DISTINCT `dinner_list`.*
      FROM `dinner_list`
      JOIN `dinnerProduct` ON `dinner_list`.`dinner_id` = `dinnerProduct`.`dinner_list` WHERE
      `dinner_list`.`main_cat` = '$search' ORDER BY `dinner_id` DESC LIMIT %s, %s", ($page-1)*$per_page, $per_page);
    }
         
      $stmt = $pdo->query($sql);
      $row = $stmt->fetchAll();

      $totalRows = count($row);
      $totalPages = ceil($totalRows/$per_page);

      $result['totalRows'] = $totalRows;
      $result['totalPages'] = $totalPages;

      $result['rows'] = $row;
      $result['test'] = $search;
      // print_r($row);
  }

// 如果有勾選 checkbox

  if(! empty($_GET['checked_check'])){
    
      $checked_check = json_decode($_GET['checked_check']);
      $result['checked_check'] = $checked_check;

      $on_off = $_GET['on'];

      if($on_off==0){
        $sql = "UPDATE `dinner_list` SET `onboard`='上架中' WHERE `dinner_id`=?";
          // $result['rows'] = $row;
        $result['check'] = '有';
      }else{
        $sql = "UPDATE `dinner_list` SET `onboard`='下架中' WHERE `dinner_id`=?";
      }
   
      $stmt = $pdo->prepare($sql);

      foreach($checked_check as $value) {
        // echo $value;
        $stmt->execute([
          $value,
        ]); 

      }

    
      
  };

  

// 餐廳的菜色 id 放入陣列 d_id_Ar
foreach ($row as $key => $value) {
  $d_id_Ar[] = $value['dinner_id'];
};

// 餐廳 id 取代為餐廳名稱
foreach ($row as $key => $value) {
    $row[$key]['restaurant_id']=$row_restaurant[$key][1];
};



// 抓菜色有的食材
// 迴圈方法抓

$sql_food = "SELECT `product_class`.`name` FROM `product_class` JOIN `dinnerproduct` ON `product_class`.`class_sid` = `dinnerproduct`.`product_class` WHERE `dinnerproduct`.`dinner_list` = ?";

$stmt_food = $pdo->prepare($sql_food);
$rows_food = [];

for($i=0; $i<count($d_id_Ar); $i++){
    $stmt_food->execute([$d_id_Ar[$i]]);
    $rows_food[] = $stmt_food->fetchAll();
};

  foreach ($rows_food as $key => $value) {
    foreach ($value as $k => $v) {
         $result['rows'][$key]['food'][]= $rows_food[$key][$k]['name'];
  }    
};

// print_r($result['rows']);

  
// print_r($row);
// exit;


// 抓菜色有的食材商品
// 迴圈方法抓

$sql_product = "SELECT `farmer_product`.`name` FROM `farmer_product` JOIN `dinnerproduct` ON `farmer_product`.`sid` = `dinnerproduct`.`farmer_product` WHERE `dinnerproduct`.`dinner_list` = ?";

$stmt_product = $pdo->prepare($sql_product);
$rows_product = [];

for($i=0; $i<count($d_id_Ar); $i++){
    $stmt_product->execute([$d_id_Ar[$i]]);
    $rows_product[] = $stmt_product->fetchAll();
};

  foreach ($rows_product as $key => $value) {
    foreach ($value as $k => $v) {
         $result['rows'][$key]['product'][]= $rows_product[$key][$k]['name'];
  }    
};

// print_r($rows_product);
  
// echo json_encode($result, JSON_UNESCAPED_UNICODE);
// exit;

// echo '<pre>';
// print_r($row);
// echo '</pre>';

// echo ($row[0]['main_ingred']);
// $newInd = [];
// foreach ($food as $key => $value) {
//     $newInd [] = $key;
// };
// print_r($newInd);

// 置換食材 id 變成名稱
// foreach ($row as $k => $v) {
//     // echo '<pre>';
//     // print_r($v);
//     // echo '</pre>';

//     $row[$k]['main_ingred'] = $food[$row[$k]['main_ingred']];
//     $row[$k]['main_ingred_replace1'] = $food[$row[$k]['main_ingred_replace1']];
//     $row[$k]['main_ingred_replace2'] = $food[$row[$k]['main_ingred_replace2']];
//     $row[$k]['main_ingred_replace3'] = $food[$row[$k]['main_ingred_replace3']];

// };

// print_r($row);

// $result['rows'] = $row;

echo json_encode($result, JSON_UNESCAPED_UNICODE);


// SQL 抓食材名稱 (對應食材內容 ID)
// SELECT p.`class_sid`, p.`name`
//      FROM `dinner_list` AS d JOIN `product_class` AS p
//      WHERE `class_sid` IN (d.`main_ingred`, d.`main_ingred_replace1`, d.`main_ingred_replace2`, d.`main_ingred_replace3`) ORDER BY class_sid ASC

// SQL 抓餐廳名稱
// SELECT r.`restaurant_id`, r.`name`
//      FROM `restaurant` AS r JOIN `dinner_list` AS d
//      WHERE r.`restaurant_id` IN (d.`restaurant_id`) ORDER BY r.restaurant_id ASC
?>





