<?php

require '../__connect_db.php';
$page_name = 'dinner_insert';
$page_title = 'dinner_insert';

include __DIR__ .'/value_match.php';

// 抓登入的餐廳的 id
$restaurant_id = $_SESSION['loginUser']['restaurant_id'];


// 圖片上傳的本機資料夾
$uploads = __DIR__. '/my_uploads/';

$set = empty($_POST['set'])? [] : $_POST['set'];
$set2 = empty($_POST['set2'])? 0 : intval($_POST['set2']);
$ingred = empty($_POST['ingred'])? 0 : intval($_POST['ingred']);

// 拿 product_class 食物名稱
$sql = "SELECT `category_sid`, `class_sid`, `name` FROM `product_class` WHERE 1";

$stmt = $pdo->query($sql);

$total_food_class = $stmt->fetchAll(PDO::FETCH_NUM);
// print_r($total_food_class);

$food = [];
foreach ($total_food_class as $k=>$v) {
    // print_r($v) ;
    $food[] = [$v[0]=>$v[2]];
};
// print_r($food);

$food_sid = [];
foreach ($total_food_class as $k=>$v) {
    // print_r($v) ;
    $food_sid[] = [$v[1]=>$v[2]];
};
// print_r($food_sid);


?>
<?php include '../__html_head.php' ?>
<?php include '../__html_body.php'   ?>
<?php include '../__html_breadCrumb.php'   ?>

<style>
        .img_wr{
            width: 200px;
            overflow: hidden;
            display:inline;
        }
        .thumb{
            width:100%;
            object-fit: cover;
        }
        .form_control{
          width: 100%;
          height: calc(1.5em + 0.75rem + 2px);
          padding: 0.375rem 0.75rem;
          font-size: 1rem;
          font-weight: 400;
          line-height: 1.5;
          color: #495057;
          background-color: #fff;
          background-clip: padding-box;
          border: 1px solid #ced4da;
          border-radius: 0.25rem;
          transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        small {
          border-radius: 3px;
          padding: 5px;
        }
</style>

  <div class="container">
    <div>
        <pre><?php
            if(! empty($_FILES)){
               var_dump($_FILES);
            }
             
            if(! empty($_POST)){
              var_dump($_POST);
            }
            
            // print_r($food_sid)
        ?>
        </pre>
    </div>


    <div class="card col-md-10">
      <div class="card-body">
        <h5 class="card-title">新增菜色表單</h5>
          <form name="dinner_form"  enctype="multipart/form-data" onsubmit="return checkForm()">

            <input type="hidden" value="<?= $restaurant_id ?>" name="restaurant_id">
            
            <select name="onboard" id="onboard" class="form_control col-md-3">
              <option value="" selected>請選擇狀態</option>
              <option value="上架中">上架</option>
              <option value="下架中">下架</option>
            </select>

            <div class="form-group col-md-6">
                <label for="">** 菜色大分類</label>
                <?php foreach ($main_cat as $k=>$v): ?>
                <input type="radio" id="main_cat_<?= $k ?>" name="main_cat" value="<?= $v ?>"><?= $v ?>
                <?php endforeach; ?>
            </div>

            <div class="form-group col-md-6">
                <label for="">** 菜色子分類</label>
                <?php foreach ($small_cat as $k=>$v): ?>
                <input type="radio" id="small_cat_<?= $k ?>" name="small_cat" value="<?= $v ?>"><?= $v ?>
                <?php endforeach; ?>
            </div>
            
            <div class="form-group">
                <label for="Email">** 菜色名稱 (15字內) (中文)</label>
                <input type="text" class="form-control" id="dinner" name="dinner">
            </div>

            <div class="form-group">
                <label for="intro">** 特色簡介 (20字內) (中文)</label>
                <textarea class="form-control" id="intro" name="intro"></textarea>
            </div>

        <div id="my_content">

             <div class="form-group col-md-12 chooseBox">
            </div>
            
        <div id="test"></div>

        </div>

        <a id="add" href="javascript:my_add();">增加食材</a>
        <a id="cut" href="javascript:my_cut();">減少食材</a>

    

            <div class="form-group">
                <label for="picture">上傳菜色照片 (最少1張，最多3張)</label>
                <!-- <button id="choose_file" onclick="choose()">選擇檔案</button> -->
                
                <input type="file" style="cursor:grab" class="form-control" id="picture" name="picture[]" multiple>選擇檔案
                <small id="pictureHelp"></small>
            </div>
            <div onclick="removePic()" style="color:gray; cursor:grab">重新選擇</div>

            <div class="form-group col-md-6">預覽照片</div>
            <div id="list" class=""></div>
           
            

            <button type="submit" class="btn btn-primary">Submit</button>

          </form>
      </div>
    </div>   
  </div>

  <script>

      // 生成食材選單
        

      let my_content = document.querySelector('#my_content');
      // let chooseBox = document.querySelectorAll('.chooseBox');

      let group = [];
      let i = 0;

      const chooseBox_str = '<div class="form-group col-md-12 chooseBox"></div>'

      const select_str = 
                `<label for="food">預設主食材</label>
                <select class="form_control col-md-3 selectBox" id="<%= id %>" name="main_ingred[]">
                <option selected value="">--選擇類別--</option>
                <?php foreach ($main_ingred_class as $k=>$v): ?>
                <option class="main_ingred_sel" id="main_ingred_sel_<?= $k ?>" value="<?= $k ?>"><?= $v ?></option>
                <?php endforeach; ?>
                </select>
                <select class="form_control col-md-3 selectName" id="<%= small_id %>"  name="main_ingred[]">
                <option id="main_food_<?= $k ?>" selected value="">--請選擇--</option>
                </select>
                <select class="form_control col-md-4 selectProduct" id="<%= tiny_id %>"  name="main_ingred[]">
                <option id="product_<?= $k ?>" selected value="">--請選擇--</option>
                </select>`
 
    
      let ppp_fn = _.template(select_str);

      let food_class = <?= json_encode($food, JSON_UNESCAPED_UNICODE); ?>;
      let food_sid = <?= json_encode($food_sid, JSON_UNESCAPED_UNICODE); ?>;
      let product = <?= json_encode($product, JSON_UNESCAPED_UNICODE); ?>;
      let product_sid =  <?= json_encode($product_sid, JSON_UNESCAPED_UNICODE); ?>;
      let product_price = <?= json_encode($product_price, JSON_UNESCAPED_UNICODE); ?>;
      let product_specification = <?= json_encode($product_specification, JSON_UNESCAPED_UNICODE); ?>;
      // console.log(product)
      let classChoose;
      let rice_class = [];
      let list = document.querySelector('#list');
      // let img_wr = document.querySelector('.img_wr');
      let chooseBox = '';
   
      for(s in food_class){
        rice_class.push(food_class[s]);
      }

      function my_add(){
          i++;
          m_str = '';
          m_str += chooseBox_str;
             
          my_content.innerHTML += m_str;

          chooseBox = document.querySelectorAll('.chooseBox')
          // console.log(chooseBox);

          group.push({
            'id':`main_ingred${i}`,
            'small_id':`main_ingred${i}_name`,
            'tiny_id':`main_ingred${i}_product`})

          // console.log(group);
   
          // 生成 select 塞進 chooseBox div 內
          for(s in group){
                item = group[s];
                strrr = ppp_fn(item);
                chooseBox[s].innerHTML = strrr;
          }

          let selectBox = document.querySelectorAll('.selectBox');
          let selectName = document.querySelectorAll('.selectName');
          let selectProduct = document.querySelectorAll('.selectProduct');
          // console.log(selectBox);
          console.log(selectProduct);

          selectBox.forEach(select=>{
              select.addEventListener('change', chooseEvent);  
                // console.log(select);
          })

          selectName.forEach(el=>{
              // console.log(el);
              el.addEventListener('change', anotherChooseEvent);
          })


      // 生成 option 塞進 product_class select 內
          function chooseEvent(e){
            // console.log(e); 
          let value = e.target.value;  
                 
          let unix = [];

          rice_class.forEach(element => {
            let food_name = element[`${value}`];
          
            if(food_name){
              unix.push(food_name);  
            }           
          });
          // console.log(unix);
          
          let c2 = '';

          for(s in food_sid){
            for(k in food_sid[s]){
              // console.log(food_sid[s][k]);
              unix.forEach(el=>{           
                if(food_sid[s][k]==el){
                  // console.log(k+el);
                   c2 += `<option value = "${k}">${el}</option>`
                }
              })
            }
          }

          for(s in group){
            if(group[s].id == e.target.id){
              // console.log(selectName[s])
              selectName[s].innerHTML = `<option>--選擇食材--</option>` + c2;
            }
          }

        };

  

      // 生成 option 塞進 farmer_product select 內
        function anotherChooseEvent(e){
            console.log(e.target.id); 
            let value = e.target.value;  

            let unix = [];

            // 拿到食材的商品品項塞進陣列 unix
            product.forEach(el=>{
              let productName = el[`${value}`];
              // console.log(productName)
              if(productName){
                unix.push(productName);
              }      
            })
           
            console.log(unix);

            let c2 = '';

            // for(s in product){
              for(k in product_sid){
                for(s in product_sid[k]){
                 
                    if(unix.indexOf(product_sid[k][s])>=0){
                      console.log(s+product_sid[k][s]);
                      c2 += `<option value = "${s}">${product_sid[k][s]} ${product_specification[k][s]} ${product_price[k][s]}元</option>`
                    }
     
                }
              }

            // console.log(c2);
            
            // 抓到的字串放進 select 裡面 生成 option
            for(s in group){
              if(group[s].small_id == e.target.id){
                // console.log(selectName[s])
                selectProduct[s].innerHTML = `<option>--選擇食材--</option>` + c2;
              }
            }

          
        };

      }

      my_add();

     
      

      function my_cut(){          
          my_content.lastChild.outerHTML = "";
          // console.log(chooseBox);
      }

      my_cut();

      
      // 光箱問答
            Notiflix.Confirm.Init({
          width: "350px",
          okButtonBackground: "#ce4e4e",
          titleColor: "#e81616",
          titleFontSize: "20px",
          fontFamily: "Arial",
          useGoogleFont: false,
      });

      function checkForm(){

        let fd = new FormData(document.dinner_form);

        fetch('dinner_insert_API.php', {
            method: 'POST',
            body: fd
        })
            .then(response=>{
                return response.json();
            })
            .then(json=>{
                console.log(json);
                Notiflix.Confirm.Show(
                  // Notice Content
                  `${json.status}`,
                  `${json.info}`,
                  '回菜色列表',
                  `${json.orto}`,
                  // ok button callback
                  function () {
                        location.href="dinner_list.php"
                  },

                  // cancel button callback
                  function() {
                    if(json.orto=='新增下一筆'){
                        location.href="dinner_insert.php"
                    }
                  }
              );
                // alert(json);
            })

        return false;
        };
      
      function removePic(){

        let img = document.querySelector('.thumb');
        // console.log(list.childNodes);

        list.childNodes[0].outerHTML="";
        removePic();    
      } 
      // console.log(list.childNodes);

  </script>
<script src="my_js/image_preview.js"></script>

<script>
  $('')
</script>

<?php include '../__html_foot.php' ?>