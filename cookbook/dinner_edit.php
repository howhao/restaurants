<?php

require '../__connect_db.php';

$page_name = 'dinner_edit';
$page_title = 'dinner_edit';

include __DIR__ .'/value_match.php';

// 圖片上傳的本機資料夾
$uploads = __DIR__. '/my_images/';

$set = empty($_POST['set'])? [] : $_POST['set'];
$set2 = empty($_POST['set2'])? 0 : intval($_POST['set2']);
$ingred = empty($_POST['ingred'])? 0 : intval($_POST['ingred']);


// 拿所有可選的 product_class 食物名稱
$sql = "SELECT `category_sid`, `class_sid`, `name` FROM `product_class` WHERE 1";

$stmt = $pdo->query($sql);

$total_food_class = $stmt->fetchAll(PDO::FETCH_NUM);
// print_r($total_food_class);

$food = [];
foreach ($total_food_class as $k=>$v) {
    // print_r($v) ;
    $food[] = [$v[0]=>$v[2]];
};
// print_r($food);

$food_sid = [];
foreach ($total_food_class as $k=>$v) {
    // print_r($v) ;
    $food_sid[] = [$v[1]=>$v[2]];
};
// print_r($food_sid);


// 以下為編輯資料的部分，拿資料庫資料預顯示
// 拿菜色資料
$sid = $_GET['sid'];

$sql_total = "SELECT * FROM `dinner_list` WHERE `dinner_id`=$sid";

$stmt_total = $pdo->query($sql_total);
$row = $stmt_total->fetch();

// 拿菜色有的食材類別與食材 (關聯式資料表)

$sql_food = "SELECT `product_category`, `product_class`, `farmer_product` FROM `dinnerproduct` WHERE `dinner_list` = $sid";

$stmt_food = $pdo->query($sql_food);
$row_food = $stmt_food->fetchAll();

// foreach ($row_food as $value): 
//   echo ($value['category_sid']==5?'selected':'');
// endforeach;

$fff = '';
foreach ($row_food as  $value) {
  $fff .= json_encode($value) . ',';
}


// 拿餐廳 id 跟名稱
$sql = "SELECT r.`restaurant_id`, r.`name`
FROM `restaurant` AS r JOIN `dinner_list` AS d
WHERE r.`restaurant_id` IN (d.`restaurant_id`) AND d.`dinner_id`=$sid ORDER BY r.restaurant_id";

$stmt_restaurant = $pdo->query($sql);
$row_restaurant = $stmt_restaurant->fetch(PDO::FETCH_NUM);


// 拿圖片檔
$image = json_decode($row['dinner_image']);


?>
<?php include '../__html_head.php' ?>
<?php include '../__html_body.php'   ?>


<style> 
        .list{
            min-width: 500px;
            display: flex;
            align-items: center;
        }
        .img_wr{
            height: 200px;
            width: 300px;
            overflow: hidden;
            display:inline;
            margin: auto;
        }
        .thumb{
            width:100%;
            height: 100%;
            object-fit: cover;
        }
        .form_control{
          width: 100%;
          height: calc(1.5em + 0.75rem + 2px);
          padding: 0.375rem 0.75rem;
          font-size: 1rem;
          font-weight: 400;
          line-height: 1.5;
          color: #495057;
          background-color: #fff;
          background-clip: padding-box;
          border: 1px solid #ced4da;
          border-radius: 0.25rem;
          transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        small {
          border-radius: 3px;
          padding: 5px;
        }
</style>

<div class="container">
    <div>
        <pre><?php
            if(! empty($_FILES)){
               var_dump($_FILES);
            }
             
            if(! empty($_POST)){
              var_dump($_POST);
            }

            // print_r($row_food);
              
        ?>
        </pre>
    </div> 


    <div class="card col-md-10">
      <div class="card-body">
        <h5 class="card-title">編輯菜色內容</h5>
          <form name="dinner_form"  enctype="multipart/form-data" onsubmit="return checkForm()">

            <input type="hidden" value="<?= $sid ?>" name="dinner_id">
            <input type="hidden" value="<?= $row['restaurant_id'] ?>">

            <div class="form-group col-md-6">
                <span>** 菜色大分類</span>

                <?php foreach ($main_cat as $k=>$v): ?>
                <input type="radio" id="main_cat_<?= $k ?>" name="main_cat" value="<?= $v ?>"
                <?= ($v == $row['main_cat']) ? 'checked' :'' ?>
                >
                <label for="main_cat_<?= $k ?>"><?= $v ?></label>
                
                <?php endforeach; ?>
            </div>

            <div class="form-group col-md-6">
                <span>** 菜色子分類</span>

                <?php foreach ($small_cat as $k=>$v): ?>
                <input type="radio" id="small_cat_<?= $k ?>" name="small_cat" value="<?= $v ?>"

                <?= ($v = $row['small_cat'])? 'checked' :'' ?>
                >
                <label for="small_cat_<?= $k ?>"><?= $v ?></label>
                <?php endforeach; ?>
            </div>
            
            <div class="form-group">
                <span>** 菜色名稱 (15字內) (中文)</span>
                <input type="text" class="form-control" id="dinner" name="dinner" placeholder="<?= $row['name'] ?>">
            </div>

            <div class="form-group">
                <span>** 特色簡介 (20字內) (中文)</span>
                <textarea class="form-control" id="intro" name="intro" placeholder="<?= $row['intro'] ?>" ></textarea>
            </div>

            <div id="my_content">

            </div>

            <a id="add" href="javascript:my_add();">增加食材</a>
            <a id="cut" href="javascript:my_cut();">減少食材</a>

            <div class="form-group">
                <label for="picture">上傳菜色照片 (最少1張，最多3張)</label>
                <!-- <button id="choose_file" onclick="choose()">選擇檔案</button> -->
                <input type="file" style="cursor:grab" class="form-control" id="picture" name="picture[]" multiple>重新上傳
                <small id="pictureHelp"></small>
            </div>
            <div onclick="removePic()" style="color:gray; cursor:grab">移除照片</div>
            <div class="form-group col-md-6">

            <div id="list" class="list">  
            <?php foreach ($image as $k => $v):?>
                <div class="img_wr"><img class="thumb" src="my_images/<?= $v ?>" id="img-<?= $k ?>"></div>        
            <?php endforeach; ?>
           
            </div>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>

          </form>
      </div>
    </div>   
  </div>

  <script>

      // 生成食材選單
        
      let my_content = document.querySelector('#my_content');
      // let chooseBox = document.querySelectorAll('.chooseBox');
      
      // 拿資料庫資料
      let row_food = <?= '[' .substr($fff,0,strlen($fff)-1).']'; ?>;
      // let test = <?= json_encode($row_food, JSON_UNESCAPED_UNICODE); ?>;
      // row_food = json_decode(row_food);
      // console.log(row_food);
      // console.log(row_food.length);


      let group = [];
      let i = 0;

      const chooseBox_str = '<div class="form-group col-md-12 chooseBox"></div>'

      const select_str = 
                `<label for="food">預設主食材</label>
                <select class="form_control col-md-3 selectBox" data-sid="<%=product_category%>" id="<%= id %>" name="main_ingred[]">
                <option value="">--選擇類別--</option>

                <?php foreach ($main_ingred_class as $k=>$v): ?>
                <option class="main_ingred_sel" id="main_ingred_sel_<?= $k ?>" value="<?= $k ?>"><?= $v ?> </option>
                <?php endforeach; ?>
                </select>
                <select class="form_control col-md-3 selectName" data-sid="<%= product_class %>" id="<%= small_id %>"  name="main_ingred[]">
                <option id="main_food_<?= $k ?>" value="">--請選擇--</option>
                </select>
                <select class="form_control col-md-4 selectProduct" data-sid="<%= farmer_product %>" id="<%= tiny_id %>"  name="main_ingred[]">
                <option id="product_<?= $k ?>" value="">--請選擇--</option>
                </select>`
 
    
      let ppp_fn = _.template(select_str);

      let food_class = <?= json_encode($food, JSON_UNESCAPED_UNICODE); ?>;
      let food_sid = <?= json_encode($food_sid, JSON_UNESCAPED_UNICODE); ?>;
      let product = <?= json_encode($product, JSON_UNESCAPED_UNICODE); ?>;
      let product_sid =  <?= json_encode($product_sid, JSON_UNESCAPED_UNICODE); ?>;
      let product_price = <?= json_encode($product_price, JSON_UNESCAPED_UNICODE); ?>;
      let product_specification = <?= json_encode($product_specification, JSON_UNESCAPED_UNICODE); ?>;
      
      let classChoose;
      let rice_class = [];
      let list = document.querySelector('#list');
      // let img_wr = document.querySelector('.img_wr');
      let count_product_class = 0;
      let count_farmer_product = 0;

      for(s in food_class){
        rice_class.push(food_class[s]);
      }

      

      function my_add(){
          i++;
          m_str = chooseBox_str;

          for(s in row_food){
            group.push({
            'id':`main_ingred${s}`,
            'small_id':`main_ingred${s}_name`,
            'tiny_id':`main_ingred${s}_product`,
            'product_category':row_food[s]['product_category'],
            'product_class':row_food[s]['product_class'],
            'farmer_product':row_food[s]['farmer_product']})
          }

          for(s in group){
             m_str += chooseBox_str;
          }
          
          my_content.innerHTML += m_str;

          let chooseBox = document.querySelectorAll('.chooseBox');
          // console.log(chooseBox);

          // console.log(group);
   
          // 生成 select 塞進 chooseBox div 內
          console.log(i)
          if(i==1){
              for(s in group){
                  // console.log(s);
                  item = group[s];
                  strrr = ppp_fn(item);
                  chooseBox[s].innerHTML += strrr;
              }
          }else{
              item = group[i+1];
              strrr = ppp_fn(item);
              chooseBox[s].innerHTML += strrr;
          }
         

          let selectBox = document.querySelectorAll('.selectBox');
          let selectName = document.querySelectorAll('.selectName');
          // console.log(selectName);
          let selectProduct = document.querySelectorAll('.selectProduct');

          selectBox.forEach(select=>{
                select.addEventListener('change', chooseEvent);  
                // console.log(select);
          })

          selectName.forEach(el=>{
              // console.log(el);
              el.addEventListener('change', anotherChooseEvent);
          })

          function chooseEvent(e){
            // console.log(e); 

          let value = e.target.value;               
          let unix = [];

          rice_class.forEach(element => {
            let food_name = element[`${value}`];
          
            if(food_name){
              unix.push(food_name);  
            }           
          });
          // console.log(unix);
          
          let c2 = '';

          for(s in food_sid){
            for(k in food_sid[s]){
              // console.log(food_sid[s][k]);
              unix.forEach(el=>{ 
                if(food_sid[s][k]==el){
                  // console.log(k+el);
                   c2 += `<option value = "${k}">${el}</option>`;
                }
              })
            }
          }

          for(s in group){
              selectName[s].innerHTML = `<option>--選擇食材--</option>` + c2;
          }
        };

        function set1(sid){
          let value = sid;  
          // console.log(value);        
          let unix = [];

          rice_class.forEach(element => {
            let food_name = element[`${value}`];
            // console.log(food_name);
          
            if(food_name){
              unix.push(food_name);  
            }           
          });

          // $.each(unix, function(key, value){
          //   console.log(value)
          // })
          // console.log(unix.length)
        
          let c2 = '';
         
            for(s in food_sid){
              for(k in food_sid[s]){
                  unix.forEach(el=>{ 
                    if(food_sid[s][k]==el){
                      // console.log(k+el);
                      c2 += `<option value = "${k}">${el}</option>`;
                    }
                  })
              }
            }

            // console.log(c2)

            selectName[count_product_class].innerHTML = `<option>--選擇食材--</option>` + c2;

            count_product_class ++;
        };


         // 生成 option 塞進 farmer_product select 內
         function anotherChooseEvent(e){
            // console.log(e.target.id); 
            let value = e.target.value;  

            let unix = [];

            // 拿到食材的商品品項塞進陣列 unix
            product.forEach(el=>{
              let productName = el[`${value}`];
              // console.log(productName)
              if(productName){
                unix.push(productName);
              }      
            })
           
            // console.log(unix);

            let c2 = '';

            // for(s in product){
              for(k in product_sid){
                for(s in product_sid[k]){
                 
                    if(unix.indexOf(product_sid[k][s])>=0){
                      // console.log(s+product_sid[k][s]);
                      c2 += `<option value = "${s}">${product_sid[k][s]} ${product_specification[k][s]} ${product_price[k][s]}元</option>`
                    }
     
                }
              }

            // console.log(c2);
            
            // 抓到的字串放進 select 裡面 生成 option
            for(s in group){
              if(group[s].small_id == e.target.id){
                // console.log(selectName[s])
                selectProduct[s].innerHTML = `<option>--選擇食材--</option>` + c2;
              }
            }         
        };

        // 生成 option 塞進 farmer_product select 內
        function set2(sid){
            let value = sid; 
            let unix = [];

            // 拿到食材的商品品項塞進陣列 unix
            product.forEach(el=>{
              let productName = el[`${value}`];
              // console.log(productName)
              if(productName){
                unix.push(productName);
              }      
            })
           
            // console.log(unix);

            let c2 = '';

            // for(s in product){
              for(k in product_sid){
                for(s in product_sid[k]){
                 
                    if(unix.indexOf(product_sid[k][s])>=0){
                      // console.log(s+product_sid[k][s]);
                      c2 += `<option value = "${s}">${product_sid[k][s]} ${product_specification[k][s]} ${product_price[k][s]}元</option>`
                    }
     
                }
              }

            // console.log(c2);
            
            // 抓到的字串放進 select 裡面 生成 option
        
             selectProduct[count_farmer_product].innerHTML = `<option>--選擇食材--</option>` + c2;
            
             count_farmer_product ++;
        };

        // 預設大類別 product_category
          $.each(group, function(key, value){
            let select = $(`#${value['id']}`);
            let option = $(select).find('option');
            // console.log(select);
            let sid = select.data('sid');
            // console.log(sid);
            $.each(option, function(key, value){
                if($(value).val()==sid){
                  $(value).attr('selected', true)
                  // chooseEvent(e, sid);
                  set1(sid);
                }
            });
          });
        
        // 預設 product_class
          $.each(group, function(key, value){
            let select = $(`#${value['small_id']}`);
            let option = $(select).find('option');
            // console.log(select);
            let sid = select.data('sid');
            // console.log(sid);
            $.each(option, function(key, value){
                if($(value).val()==sid){
                  $(value).attr('selected', true)
                  set2(sid);
                }
            });
          });

          
        // 預設 farmer_product
        $.each(group, function(key, value){
            let select = $(`#${value['tiny_id']}`);
            let option = $(select).find('option');
            // console.log(select);
            let sid = select.data('sid');
            // console.log(sid);
            $.each(option, function(key, value){
                if($(value).val()==sid){
                  $(value).attr('selected', true)
                }
            });
          });

      }
 
    
      my_add();
 
          

      // $('#my_content select').find('option'))
        

      function my_cut(){          
          $('#my_content .chooseBox:last-child').remove();
      }


      // 光箱問答
      Notiflix.Confirm.Init({
          width: "350px",
          okButtonBackground: "#ce4e4e",
          titleColor: "#e81616",
          titleFontSize: "20px",
          fontFamily: "Arial",
          useGoogleFont: false,
      });

      function checkForm(){

        let fd = new FormData(document.dinner_form);

        fetch(`dinner_edit_API copy.php?`, {
            method: 'POST',
            body: fd
        })
            .then(response=>{
                return response.json();
            })
            .then(json=>{
                console.log(json);
                // console.log(json);
                //   if(confirm(json.status)){
                //     setTimeout(() => {
                //     location.href="dinner_list.php"
                //   }, 500);
                //   }else{
                //     alert('請重新選擇檔案');
                //   }

                  Notiflix.Confirm.Show(
                  // Notice Content
                  `${json.info}`,
                  `${json.status}`,
                  `${json.to}`,
                  `${json.orto}`,
                  // ok button callback
                  function () {
                    setTimeout(() => {
                        location.href="dinner_list.php"
                    }, 500);
                  },

                  // cancel button callback
                  function() {
                    // img.remove();
                  }
              );
            })

        return false;
        };

          
      function removePic(){
        console.log(list.childNodes);
        for(i=0; i<list.childNodes.length; i++){
            list.childNodes[i].outerHTML="";
            // removePic();    
        }
        
        } 
        // console.log(list.childNodes);



  </script>
<script src="my_js/image_preview.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $(function(){
    $('#list').sortable();
    $('#list').disableSelection();
  })
</script>

<?php include '../__html_foot.php' ?>








