

<script src="../assets/js/popper.min.js"></script>
<script src="../assets/js/bootstrap.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="../assets/js/dropzone.js" ></script>
<script src="../assets/js/paper-dashboard.js?v=2.0.0" type="text/javascript"></script>
<script src="../assets/js/lightbox.js"></script>
<script>
            lightbox.option({
            'wrapAround': true,
            'albumLabel': "第 %1 張 共 %2 張",
            })
</script>
</body>

</html>